import sqlite3
from time import gmtime, strftime
import os
import re
import time

# Usage
# s = Storage("chunk_2.db")
# s.create_table("Sheet 1")
# s.add_cell('Sheet 1', 'A', 1, 'SAMPLE 1')
# s.get_cell('Sheet 1', 'A', 2)
# s.update_cell('Sheet 1', 'A', 3, 'SAMPLE 10')
# s.remove_cell('Sheet 1', 'A', 3)


class Storage:
    def __init__(self, db_name):
        self.db_name = db_name
        self.conn = sqlite3.connect(self.db_name, check_same_thread=False)
        self.cursor = self.conn.cursor()

    def create_table(self, table_name):
        self.cursor.execute('''CREATE TABLE IF NOT EXISTS `{}` (row text, column real, value text, inserted_at text)'''.format(table_name))
        self.cursor.execute('''CREATE INDEX IF NOT EXISTS `{}_index` ON `{}` (row, column)'''.format(table_name, table_name))

        self.conn.commit()

    def add_cells(self, table_name, cells, value):
        start = time.clock()

        for (row, column) in self.get_cell_range_list(cells):
            self.add_cell(table_name, row, column, value)
        print(time.clock() - start)

    def add_cell(self, table_name, row, column, value):
        inserted_at = strftime("%Y-%m-%d %H:%M:%S", gmtime())
        if self.get_cell(table_name, row, column):
            self.update_cell(table_name, row, column, value)
        else:
            self.cursor.execute("INSERT INTO `{}` VALUES (?, ?, ?, ?)".format(table_name), [row, column, value, inserted_at])
            self.conn.commit()

    def remove_cells(self, table_name, cells):
        for (row, column) in self.get_cell_range_list(cells):
            self.remove_cell(table_name, row, column)

    def remove_cell(self, table_name, row, column):
        self.cursor.execute("DELETE FROM `{}` where row=? and column=?".format(table_name), [row, column])
        self.conn.commit()

    def update_cells(self, table_name, cells, new_value):
        for (row, column) in self.get_cell_range_list(cells):
            self.update_cell(table_name, row, column, new_value)

    def update_cell(self, table_name, row, column, new_value):
        self.cursor.execute("UPDATE `{}` set value=? where row=? and column=?".format(table_name), [new_value, row, column])
        self.conn.commit()

    def get_cells(self, table_name, cells):
        query_cells = []
        # for (row, column) in self.get_cell_range_list(cells):
        for (row, column) in cells:
            query_cells.append("(row='{}' and column={})".format(row, column))

        query_cells = " OR ".join(query_cells)
        self.cursor.execute("SELECT value FROM `{}` where {}".format(table_name, query_cells))
        self.conn.commit()
        rows = self.cursor.fetchall()
        values = []
        for row in rows:
            (value,) = row
            values.append(value)
        return values

    def get_cell(self, table_name, row, column):
        self.cursor.execute("SELECT value FROM `{}` where row=? and column=?".format(table_name), [row, column])
        self.conn.commit()
        row = self.cursor.fetchone()
        value = None
        if row:
          (value,) = row
        return value

    def get_cell_range_list(self, cells):
        [r1, c1, r2, c2] = self.get_cell_range(cells)
        return self.expand_cell_range([r1, c1, r2, c2])

    def get_cell_range(self, s):
        print(s)
        split = s.split(":")
        if len(split) == 1:
            return self.parse_cell(split[0]) + self.parse_cell(split[0])
        else:
            return self.parse_cell(split[0]) + self.parse_cell(split[1])

    def expand_cell_range(self, points):
        [r1, c1, r2, c2] = points
        c2 = int(c2)
        c1 = int(c1)
        r1 = col_to_num(r1)
        r2 = col_to_num(r2)
        cells = []
        for i in range(r1 - r2 + 1):
            for j in range(c2 - c1 + 1):
                cells.append((num_to_col(i+r1), j+c1))
        return cells

    def parse_cell(self, cell):
        print(cell)
        regex = re.search("(\D+)(\d+)", cell)
        return [regex.group(1), regex.group(2)]

    def close(self):
        self.conn.close()


def num_to_col(n):
    name = ''
    while n > 0:
        n, r = divmod(n - 1, 26)
        name = chr(r + ord('A')) + name
    return name


def col_to_num(name):
    n = 0
    for c in name:
        n = n * 26 + 1 + ord(c) - ord('A')
    return n