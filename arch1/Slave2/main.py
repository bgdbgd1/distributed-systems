import socket
import pickle
from threading import Thread, Lock
from Slave import Slave
from db import Storage
import re
from functools import reduce 
import traceback

print('Starting storage engine...')
db_name = 'chunk_2'
table_name = 'Sheet 1'
storage = Storage(db_name)
storage.create_table(table_name)
storage.add_cells(table_name, "A101:A200", 2)

max_rows = 100

list_slaves = [Slave(1, "127.0.0.1", 5080, 1, 100),
               Slave(2, "127.0.0.1", 5090, 101, 200)]
HOST = '127.0.0.1'
PORT = 5090
clients = {}
addresses = {}
locker = Lock()

BUFSIZ = 1024
ADDR = (HOST, PORT)
SERVER = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
SERVER.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
SERVER.bind(ADDR)


def isTuple(x): return type(x) == tuple


def flatten(T):
    if not isTuple(T): return (T,)
    elif len(T) == 0: return ()
    else: return flatten(T[0]) + flatten(T[1:]) 


def SUM(*argv):
    data = flatten(argv)
    return reduce(lambda x,y: int(x) + int(y), data)


def INDEX(items, val):
    try:
        return items.index(val)
    except:
        return None


def MATCH(items, i):
    try:
        return items[i]
    except:
        return None


def AVG(*argv):
    return SUM(argv)/len(argv)


def MIN(*argv):
    return min(argv)


def MAX(*argv):
    return max(argv)


def NOT(a):
    return not a


def AND(*argv):
    for a in argv:
        if int(a) == 0:
            return 0
    return 1


def OR(*argv):
    for a in argv:
        if int(a) == 1:
            return 1
    return 0


def XOR(*argv):
    result = 0
    for a in argv:
        result ^= int(a)
    return result


def NAND(*argv):
    for a in argv:
        if int(a) == 0:
            return 1
    return 0


def NOR(*argv):
    for a in argv:
        if int(a) == 1:
            return 0
    return 1


def EQ(a, b):
    return a == b


def LT(a, b):
    return int(a) < int(b)


def GT(a, b):
    return int(a) > int(b)


def LE(a, b):
    return int(a) <= int(b)


def GE(a, b):
    return int(a) >= int(b)


def update_formula_match(r):
    match = r.group(0)
    return get_cells(match)


def process_result_formula(formula):
    if type(formula) == list: 
        formula = ",".join(formula)
    if type(formula) == tuple:
        formula = formula[0]
    formula = re.sub("(?:\".*?\")|(([A-Z]+\d+\:[A-Z]+\d+)|([A-Z]+\d+))|(?:\'.*?\')", update_formula_match, formula)
    try:
        e = eval(formula)
        if type(e) == tuple:
            e = ','.join(str(a) for a in e)
        if type(e) == int:
            e = str(e)
        return e
    except Exception:
        traceback.print_exc()
        return formula


def get_cells(cells):
    rng = get_cell_range(cells)
    # # cells_range = get_cell_range_list(cells)
    # # result = storage.get_cells(table_name, cells_range)
    minRow = int(rng[1])
    maxRow = int(rng[3])

    result = []
    slaves_to_call = []
    max_found = False
    for slave in list_slaves:
        if minRow > slave.maxPos:
            continue
        else:
            if maxRow < slave.maxPos:
                if(slave.maxPos - maxRow)//100 == 0:
                    if minRow > slave.minPos:
                        slaves_to_call.append(Slave(slave.id, slave.host, slave.port, minRow, maxRow))
                    else:
                        slaves_to_call.append(Slave(slave.id, slave.host, slave.port, slave.minPos, maxRow))
                    max_found = True
                elif not max_found:
                    if minRow > slave.minPos:
                        slaves_to_call.append(Slave(slave.id, slave.host, slave.port, minRow, slave.maxPos))
                    else:
                        slaves_to_call.append(Slave(slave.id, slave.host, slave.port, slave.minPos, slave.maxPos))

    for slave in slaves_to_call:
        if slave.port == PORT:
            temp_cells = rng[0] + str(slave.minPos) + ":" + rng[2] + str(slave.maxPos)
            cells_range = get_cell_range_list(temp_cells)
            for cell in storage.get_cells(table_name, cells_range):
                result.append(cell)
        else:
            to_send = "GET?:" + rng[0] + str(slave.minPos) + ":" + rng[2] + str(slave.maxPos)
            slave_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            slave_socket.connect((slave.host, slave.port))
            slave_socket.send(bytes(to_send, "utf8"))
            receive = slave_socket.recv(BUFSIZ)
            decoded_receive = pickle.loads(receive)
            for rcv in decoded_receive:
                result.append(rcv)

            # call each slave and retrieve its values

    return process_result(result)


def get_cell_range_list(cells):
    [r1, c1, r2, c2] = get_cell_range(cells)
    return expand_cell_range([r1, c1, r2, c2])


def get_cell_range(s):
    print(s)
    split = s.split(":")
    if len(split) == 1:
        return parse_cell(split[0]) + parse_cell(split[0])
    else:
        return parse_cell(split[0]) + parse_cell(split[1])


def parse_cell(cell):
    print(cell)
    regex = re.search("(\D+)(\d+)", cell)
    return [regex.group(1), regex.group(2)]


def expand_cell_range(points):
    [r1, c1, r2, c2] = points
    c2 = int(c2)
    c1 = int(c1)
    r1 = col_to_num(r1)
    r2 = col_to_num(r2)
    cells = []
    for i in range(r1 - r2 + 1):
        for j in range(c2 - c1 + 1):
            cells.append((num_to_col(i+r1), j+c1))
    return cells


def num_to_col(n):
    name = ''
    while n > 0:
        n, r = divmod(n - 1, 26)
        name = chr(r + ord('A')) + name
    return name


def col_to_num(name):
    n = 0
    for c in name:
        n = n * 26 + 1 + ord(c) - ord('A')
    return n


def process_result(temp_result):
    if temp_result is None:
        return
    try:
        result = process_result_formula(temp_result)
        return result
    except:
        traceback.print_exc()
        return 


def handle_client(client):
    global storage
    global table_name
    while 1:
        result = []
        data = client.recv(1024)
        decoded_data = data.decode()
        arguments = decoded_data.split('?:')
        cells = arguments[1]
        result = None
        if arguments[0] == 'GET':
            result = []
            result = get_cells(cells)
            # for r in get_cells(cells):
            #     result.append(process_result(r))
        else:
            if arguments[0] == 'UPDATE':
                value = arguments[2]
                storage.update_cells(table_name, cells, value)
                result = "UPDATED"

            elif arguments[0] == 'INSERT':
                value = arguments[2]
                storage.add_cells(table_name, cells, value)
                result = "INSERTED"

            elif arguments[0] == 'DELETE':
                storage.remove_cells(table_name, cells)
                result = "DELETED"

            elif arguments[0] == 'QUIT':
                client.close()
                del addresses[client]
                break
        
        client.send(pickle.dumps(result))


def accept_incoming_connections():
    while 1:
        client, client_address = SERVER.accept()
        addresses[client] = client_address
        Thread(target=handle_client, args=(client,)).start()


def main():
    SERVER.listen(5)
    print('Waiting for connection...')

    ACCEPT_THREAD = Thread(target=accept_incoming_connections)
    ACCEPT_THREAD.start()
    ACCEPT_THREAD.join()
    SERVER.close()


main()
